package org.arpameeting.phonebrowser.conference;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.arpameeting.phonebrowser.sip.AsteriskSIPServer;

@XmlRootElement
public class Account
{
	private String name;
	
	private String status;
	
	public Account() {}
	
	/**
	 * 
	 * @param id
	 */
	public Account(String name)
	{
		AsteriskSIPServer sipServer = AsteriskSIPServer.getInstance();
		this.setName(name);
		this.setStatus(sipServer.checkUser(name).toString());
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
