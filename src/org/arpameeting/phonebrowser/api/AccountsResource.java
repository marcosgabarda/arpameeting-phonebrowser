package org.arpameeting.phonebrowser.api;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import org.arpameeting.phonebrowser.conference.Account;
import org.arpameeting.phonebrowser.sip.AsteriskSIPServer;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

@Path("/accounts")
public class AccountsResource 
{
	@Context
	Request request;
	@Context
	UriInfo uriInfo;
	
	@GET
	@Produces({MediaType.APPLICATION_XML})
	public List<Account> getAccounts()
	{
		List<Account> acounts = new ArrayList<Account>();
		for (Integer i = 1000 ; i < 9999; i++)
		{
			acounts.add(new Account(i.toString()));
		}
		return acounts;
	}
	
	/**
	 * Reserva una cuenta.
	 * 
	 * @return
	 */
	@POST
	@Path("allocate")
	@Produces(MediaType.APPLICATION_XML)
	public Account allocate()
	{
		AsteriskSIPServer sipServer = AsteriskSIPServer.getInstance();
		String name = sipServer.catchUser();
		return new Account(name);
	}
	
	/**
	 * Libera una cuenta.
	 * 
	 * <account>
	 *     <name><name>
	 * </account>
	 */
	@POST
	@Path("free")
	@Produces(MediaType.APPLICATION_XML)
	@Consumes(MediaType.APPLICATION_XML)
	public void free(Document content)
	{
		AsteriskSIPServer sipServer = AsteriskSIPServer.getInstance();
		NodeList names = content.getElementsByTagName("id");
		for (int i = 0; i < names.getLength(); i++)
		{
			String name = names.item(0).getNodeValue();
			sipServer.releaseUser(name);
		}
	}
}
